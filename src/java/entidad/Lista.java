/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;

/**
 *
 * @author Faby
 */

public class Lista implements Serializable {
    private Integer idBodega;
    private String estado;
    private String municipio;
    private String localidad;
    private Integer idVenta;
    private String rfcCliente;
    private Integer idMaq;
    private String nomMaq;
    private Integer cantidad;
    private Double peso;
    private Double totalPeso;
    public Lista(){
        
    }
    public Lista(Integer idBodega, String estado, String municipio, String localidad, Integer idVenta, String rfcCliente, Integer idMaq, String nomMaq, Integer cantidad, Double peso, Double totalPeso) {
        this.idBodega = idBodega;
        this.estado = estado;
        this.municipio = municipio;
        this.localidad = localidad;
        this.idVenta = idVenta;
        this.rfcCliente = rfcCliente;
        this.idMaq = idMaq;
        this.nomMaq = nomMaq;
        this.cantidad = cantidad;
        this.peso = peso;
        this.totalPeso = totalPeso;
    }

    public Integer getIdBodega() {
        return idBodega;
    }

    public void setIdBodega(Integer idBodega) {
        this.idBodega = idBodega;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public String getRfcCliente() {
        return rfcCliente;
    }

    public void setRfcCliente(String rfcCliente) {
        this.rfcCliente = rfcCliente;
    }

    public Integer getIdMaq() {
        return idMaq;
    }

    public void setIdMaq(Integer idMaq) {
        this.idMaq = idMaq;
    }

    public String getNomMaq() {
        return nomMaq;
    }

    public void setNomMaq(String nomMaq) {
        this.nomMaq = nomMaq;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getTotalPeso() {
        return totalPeso;
    }

    public void setTotalPeso(Double totalPeso) {
        this.totalPeso = totalPeso;
    }
    
}
