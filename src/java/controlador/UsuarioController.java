package controlador;

import clarkewright.ClarkeWright;
import clarkewright.Cliente;
import clarkewright.Colonia;
import clarkewright.Entrega;
import clarkewright.Nodo;
import clarkewright.Ruta;
import entidad.Usuario;
import controlador.util.JsfUtil;
import controlador.util.PaginationHelper;
import entidad.Bodega;
import entidad.Lista;
import java.io.File;
import java.io.IOException;
import session.UsuarioFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem; 
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ManagedBean(name = "usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

    private Usuario current;
    private DataModel items = null;
    @EJB
    private session.UsuarioFacade ejbFacade;
    @EJB    
    private session.ClienteFacade ejbFacadeCli;
    @EJB
    private session.ColoniasFacade ejbFacadeCol;
    @EJB
    private session.BodegaFacade ejbFacadeBod;
    @EJB
    private session.EntragaFacade ejbFacadeEnt;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    @Size(min=3, message="Debe ser minimo 3 caracteres")
    @Pattern(regexp="^[a-zA-Z0-9]*$" , message="Solo numeros y letras ")
    private String login;
    @Size(min=3, message="Debe ser minimo 3 caracteres")
    private String pwd;
    private List<Lista> lista;
    private List<Entrega> relacion_ent;
    
    JasperPrint jasperPrint;

    public List<Entrega> getRelacion_entrega() {
        return relacion_ent;
    }

    public void setRelacion_entrega(List<Entrega> relacion_entrega) {
        this.relacion_ent = relacion_entrega;
    }
    
    
    public void init() throws JRException
    {
        Collection <Lista> o=(Collection <Lista> )lista;
        JRBeanCollectionDataSource beanCollectionDataSource=new JRBeanCollectionDataSource(o);
        String ruta="C:\\Users\\Faby\\Desktop\\NUEVO\\trescinco\\src\\java\\report\\reporte.jasper";
        String reportPath1=FacesContext.getCurrentInstance().getExternalContext().getRealPath("/report/reporte.jasper");
        jasperPrint=JasperFillManager.fillReport(ruta, new HashMap(), beanCollectionDataSource);
    }
    
    public void PDF() throws JRException, IOException
    {
        init();
        HttpServletResponse httpServletResponse=(HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment;filename=report.pdf");
        ServletOutputStream servletOutputStream=httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        FacesContext.getCurrentInstance().responseComplete();
    }
    public List<Lista> getLista() {
        lista=this.getFacade().PorEntregar(current.getBodega().getId(),"06/4/2015") ;
        
        return lista;
    }

    public void setLista(List<Lista> lista) {
        this.lista = lista;
    }
    
    public String acceso() {
        try {
            Usuario usu=getFacade().findUser(getLogin(),getPwd());
            current=usu;
            System.out.println("*********************************");
            System.out.println(usu.toString());
            System.out.println(getLogin());
            System.out.println(getPwd());
            return "principal";
        }catch(Exception e){
            System.out.println("Error no se equivoco en el usuario o contraseña");
            return "index";
        }
           
    }
    
    public String ruta() {
        
        Bodega bodega=ejbFacadeBod.findById(current.getBodega().getId());
        double bodegaLat=bodega.getLatitud();
        double bodegaLon=bodega.getLongitud();
       
        ArrayList<Nodo> entregas=new ArrayList<>();
       
        //Todoas las colonias que se entregen al dia siguiente (unique)
        //Lista de id_colonia, latitud, longitud                
        List<Colonia> colonias=ejbFacadeCol.getColonias(current.getBodega().getId(), "06/4/2015");
       
       
        for (int i = 0; i < colonias.size(); i++)        
        {            
            double peso=0;
            double tiempo=0;
            //Todos los clientes de la colonia que se entregen al dia sih¿guiente
            //Lista de clientes (peso, cantidad) donde id_colonia=colonias.get(i).getIDColonia()
            List<Cliente> clientes=ejbFacadeCli.getClientes(current.getBodega().getId(), "06/4/2015", colonias.get(i).getId());
            for (int j = 0; j < clientes.size(); j++)
            {
                peso=peso+clientes.get(j).getPeso();
                if(clientes.get(j).getCantidad()>1)
                    tiempo=tiempo+0.41;
                else
                    tiempo=tiempo+0.33;
            }                        
            entregas.add(new Nodo(i + 1,colonias.get(i).getLatitud(),colonias.get(i).getLongitud(),peso, tiempo, colonias.get(i).getId()));
       
        }        
       
        ClarkeWright clarkewright=new ClarkeWright();
        ArrayList<Ruta> rutas=clarkewright.clarkeWright(entregas, bodegaLat, bodegaLon);
        List<Entrega> relacion_entregas=ejbFacadeEnt.getEntregas(current.getBodega().getId(), "06/4/2015");
        
        for (Ruta ruta : rutas)
        {   
            if(ruta.getNodos().size() > 0)
            {
                int id_camion=ruta.getId();                            

                for (Nodo nodo : ruta.getNodos())
                {                    
                    for (Entrega relacion_entrega : relacion_entregas)
                    {
                        if(nodo.getColonia() == relacion_entrega.getId_colonia())
                        {
                            relacion_entrega.setId_camion(id_camion);
                            relacion_entrega.setH_entrega(nodo.getTime());
                        }
                    }
                }                        
            }
        }
        relacion_ent=relacion_entregas;
        return "ruta";
    }
   
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    public UsuarioController() {
    }

    public Usuario getSelected() {
        if (current == null) {
            current = new Usuario();
            selectedItemIndex = -1;
        }
        return current;
    }

    private UsuarioFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Usuario();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuarioCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuarioUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuarioDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
  
         getFacade().findOneResult("sd",null);
         int count =getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }
    
    @FacesConverter(forClass = Usuario.class)
    public static class UsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuarioController controller = (UsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuarioController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuario) {
                Usuario o = (Usuario) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Usuario.class.getName());
            }
        }
        
    }

}
