/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clarkewright;

/**
 *
 * @author Aarón Velasco
 */
public class Entrega
{
    private int id_camion;
    private String id_cliente;
    private int id_colonia;
    private String calle;
    private String numero;
    private String colonia;
    private String maquinaria;
    private int cantidad;
    private double peso;
    private int h_entrega;

    public Entrega()
    {
        this.id_camion = 1;        
        this.h_entrega = 0;
    }            

    public int getId_camion()
    {
        return id_camion;
    }

    public void setId_camion(int id_camion)
    {
        this.id_camion = id_camion;
    }

    public String getId_cliente()
    {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente)
    {
        this.id_cliente = id_cliente;
    }

    public int getId_colonia()
    {
        return id_colonia;
    }

    public void setId_colonia(int id_colonia)
    {
        this.id_colonia = id_colonia;
    }
        
    public String getCalle()
    {
        return calle;
    }

    public void setCalle(String calle)
    {
        this.calle = calle;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public String getColonia()
    {
        return colonia;
    }

    public void setColonia(String colonia)
    {
        this.colonia = colonia;
    }

    public String getMaquinaria()
    {
        return maquinaria;
    }

    public void setMaquinaria(String maquinaria)
    {
        this.maquinaria = maquinaria;
    }

    public int getCantidad()
    {
        return cantidad;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad = cantidad;
    }

    public double getPeso()
    {
        return peso;
    }

    public void setPeso(double peso)
    {
        this.peso = peso;
    }

    public int getH_entrega()
    {
        return h_entrega;
    }

    public void setH_entrega(int h_entrega)
    {
        this.h_entrega = h_entrega;
    }        
}
