package clarkewright;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
public class Nodo
{
    private int id;
    private double latitud;
    private double longitud;
    private double peso;
    private double tiempo;
    private int colonia;    
    private int time;
    
    public Nodo(int id, double latitud, double longitud, double peso, double tiempo, int colonia)
    {
        this.id=id;
        this.latitud=latitud;
        this.longitud=longitud;
        this.peso=peso;
        this.colonia=colonia;
        this.tiempo=tiempo;        
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
            
    public double getLatitud()
    {
        return latitud;
    }

    public void setLatitud(double latitud)
    {
        this.latitud = latitud;
    }

    public double getLongitud()
    {
        return longitud;
    }

    public void setLongitud(double longitud)
    {
        this.longitud = longitud;
    }

    public double getPeso()
    {
        return peso;
    }

    public void setPeso(double peso)
    {
        this.peso = peso;
    } 

    public double getTiempo()
    {
        return tiempo;
    }

    public void setTiempo(double tiempo)
    {
        this.tiempo = tiempo;
    }

    public int getColonia()
    {
        return colonia;
    }

    public void setColonia(int colonia)
    {
        this.colonia = colonia;
    }

    public int getTime()
    {
        return time;
    }

    public void setTime(int time)
    {
        this.time = time;
    }        
}
