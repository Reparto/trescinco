package clarkewright;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aarón Velasco
 */
public class Ruta
{
    private int id;
    private ArrayList<Nodo> nodos;
    private double distancia;
    private double tiempo;        
    private double peso;
    private double velocidad;    
    private int h_salida;
      
    public Ruta(int id)
    {
        this.id=id;
        nodos=new ArrayList<>();
        distancia=0;
        tiempo=0;
        peso=0;
        velocidad=40; 
        h_salida=900;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }        

    public double getDistancia()
    {
        return distancia;
    }

    public void setDistancia(double distancias [][])
    {
        double dist=distancias[0][nodos.get(0).getId()]+distancias[0][nodos.get(nodos.size()-1).getId()];        
        for (int i = 0; i < nodos.size() - 1; i++)
        {
            if(nodos.get(i).getId()<nodos.get(i+1).getId())
            {                
                dist=dist+distancias[nodos.get(i).getId()][nodos.get(i+1).getId()];
            }
            else
            {                
                dist=dist+distancias[nodos.get(i+1).getId()][nodos.get(i).getId()];
            }
        }
        this.distancia = dist;
        tiempo=distancia/velocidad;
        setTiempo();
        setPeso();
        setHoras(distancias);
    }

    public double getTiempo()
    {
        return tiempo;
    }

    public void setTiempo()
    {
        for (Nodo nodo : nodos)
        {
            this.tiempo = this.tiempo + nodo.getTiempo();
        }        
    }

    public double getPeso()
    {
        return peso;
    }

    private void setPeso()
    {
        this.peso=0;
        for (Nodo nodo : nodos)
        {
            this.peso=this.peso+nodo.getPeso();
        }
    }    

    public double getVelocidad()
    {
        return velocidad;
    }

    public void setVelocidad(double velocidad)
    {
        this.velocidad = velocidad;
    }                

    public int getH_salida()
    {
        return h_salida;
    }

    public void setH_salida(int h_salida)
    {
        this.h_salida = h_salida;
    }
            
    public void addNodo(Nodo nodo)
    {
        if(!siExiste(nodo.getId()))
        {
            nodos.add(nodo);            
        }
    }
    
    public void addNodo(int posicion, Nodo nodo)
    {
        if(!siExiste(nodo.getId()))
        {
            nodos.add(posicion, nodo);            
        }
    }
    
    public void delNodo(Nodo nodo)
    {
        if(siExiste(nodo.getId()))
            nodos.remove(nodo);
    }
    
    public ArrayList<Nodo> getNodos()
    {
        return nodos;
    }        
    
    private boolean esPrimero(int id)
    {        
        return nodos.get(0).getId()==id;
    }
    
    private boolean esUltimo(int id)
    {
        return nodos.get(nodos.size() - 1).getId()==id;
    }
    
    public int iniciOFin(int id)
    {               
        for (Nodo nodo : nodos)            
        {
            if (nodo.getId() == id)
            {
                if(esPrimero(id) && nodos.size()>1)
                    return 1;                    
                else if(esUltimo(id))
                    return 2;
            }                        
        }
        return 0;
    }
    private boolean siExiste(int id)
    {                
        for (Nodo nodo : nodos)            
        {
            if(nodo.getId() == id)
                return true;
        }        
        return false;
    }
    
    public void setHoras(double distancias [][])
    {
        double dist=distancias[0][nodos.get(0).getId()];
        double tiem=dist/velocidad*100;
        nodos.get(0).setTime(h_salida + (int)tiem);
        for (int i = 0; i < nodos.size() - 1; i++)
        {
            if(nodos.get(i).getId()<nodos.get(i+1).getId())
                dist=distancias[nodos.get(i).getId()][nodos.get(i+1).getId()];                
            else
                dist=distancias[nodos.get(i+1).getId()][nodos.get(i).getId()];                
            
            tiem=dist/velocidad*100;
            nodos.get(i+1).setTime(h_salida + (int)tiem + (nodos.get(i).getTime() - h_salida));
        }        
    }
}
