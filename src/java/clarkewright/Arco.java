package clarkewright;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aarón Velasco
 */
public class Arco
{
    private int id;
    private int nodoId1;    
    private int nodoId2;
    private double ahorro;    
    public Arco next;
    
    public Arco(int nodoId1, int nodoId2, double ahorro)
    {
        this.nodoId1=nodoId1;
        this.nodoId2=nodoId2;
        this.ahorro=ahorro;
    }

    public int getId()
    {
        return id;
    }        

    public void setId(int id)
    {
        this.id = id;
    }        
    
    public int getNodoId1()
    {
        return nodoId1;
    }

    public void setNodoId1(int nodo1)
    {
        this.nodoId1 = nodo1;
    }

    public int getNodoId2()
    {
        return nodoId2;
    }

    public void setNodoId2(int nodo2)
    {
        this.nodoId2 = nodo2;
    }

    public double getAhorro()
    {
        return ahorro;
    }

    public void setAhorro(double ahorro)
    {
        this.ahorro = ahorro;
    }          
}
