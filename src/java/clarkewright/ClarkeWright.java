package clarkewright;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aarón Velasco
 */
public class ClarkeWright 
{
    ArrayList<Nodo> entregas=new ArrayList<>();
    ArrayList<Ruta> rutas=new ArrayList<>();    
    ArcoList arcos=new ArcoList();
        
    public ArrayList<Ruta> clarkeWright(ArrayList<Nodo> entregas, double bodegaLat, double bodegaLon) 
    {    
        this.entregas=entregas;        
        
        int num_entregas=entregas.size() + 1;
        double distancias [][]= new double[num_entregas][num_entregas];
        
        for (int i = 1; i < num_entregas; i++)
        {            
            distancias[0][i]=distance(bodegaLat, bodegaLon, entregas.get(i-1).getLatitud(), entregas.get(i-1).getLongitud());
        }
        
        for (int i = 1; i < num_entregas; i++)
        {
            for (int j = i + 1; j < num_entregas; j++)
            {
                distancias[i][j]=distance(entregas.get(i-1).getLatitud(), entregas.get(i-1).getLongitud(), entregas.get(j-1).getLatitud(), entregas.get(j-1).getLongitud());
            }
        }                
        
        for (Nodo nodo : entregas)
        {            
            Ruta ruta=new Ruta(nodo.getId());            
            ruta.addNodo(nodo);            
            ruta.setDistancia(distancias);            
            rutas.add(ruta);            
        }                
                
        int num_rutas=rutas.size();
        
        for (int i = 0; i < num_rutas; i++)
        {
            for (int j = i + 1; j < num_rutas; j++)
            {
                double R=rutas.get(i).getDistancia() + rutas.get(j).getDistancia();                
                double N=distancias[0][i+1]+distancias[0][j+1]+distancias[i+1][j+1];
                double ahorro=R - N;
                if(ahorro > 0)
                {                    
                    arcos.addArco(new Arco(entregas.get(i).getId(), entregas.get(j).getId(), ahorro));
                }
            }
        }
        arcos.imprimir();                
        
        calculaRuta(arcos, distancias, rutas);                                       
        
        printRoute(rutas);        
        return rutas;
    }    
    
    private void printRoute(ArrayList<Ruta> routes)
    {
        for (Ruta ruta : routes)
        {            
            for (Nodo nodo : ruta.getNodos())
            {
                System.out.print(nodo.getId()+"\t"+nodo.getPeso()+"\t"+nodo.getTime());
            }            
        }
    }
           
    private double distance(double lat1, double lon1, double lat2, Double lon2)
    {
        // TODO Auto-generated method stub
        final int R = 6371; // Radious of the earth       
        Double latDistance = toRad(lat2 - lat1);
        Double lonDistance = toRad(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        Double distance = R * c;

        return distance;
    }

    private double toRad(Double value)
    {
        return value * Math.PI / 180;
    }
    
    private void calculaRuta(ArcoList arcos, double distancias [][], ArrayList<Ruta> routes)
    {
        ArcoList tmp=arcos;
        //while (tmp.getInicial() != null)        
        for (int i = 0; i < 2; i++)        
        {
            for (Ruta ruta : routes)
            {            
                for (Nodo nodo : ruta.getNodos())
                {
                    Ruta aux1, aux2;
                    double R;
                    double N;
                    double Ahorro;                    
                    if(tmp.getInicial() != null && nodo.getId()==tmp.getInicial().getNodoId1())
                    {                        
                        switch(ruta.iniciOFin(nodo.getId()))
                        {
                            case 1:
                                aux1=getRuta(ruta.getId(), tmp.getInicial().getNodoId2());
                                if(aux1 != null && aux1.getNodos().size() == 1)
                                {
                                    R = aux1.getDistancia() + ruta.getDistancia();
                                    aux2=ruta;
                                    aux2.addNodo(0, entregas.get(tmp.getInicial().getNodoId2() - 1));
                                    aux2.setDistancia(distancias);
                                    N=aux2.getDistancia();
                                    Ahorro=R-N;
                                    if(Ahorro > 0)
                                    {
                                        delRuta(aux2.getId(), tmp.getInicial().getNodoId2());
                                        if(aux2.getTiempo()<12)
                                        {
                                            ruta=aux2;                                            
                                        }                                        
                                    }
                                }
                                break;
                            case 2:
                                aux1=getRuta(ruta.getId(), tmp.getInicial().getNodoId2());
                                if(aux1 != null && aux1.getNodos().size() == 1)
                                {
                                    R = aux1.getDistancia() + ruta.getDistancia();
                                    aux2=ruta;
                                    aux2.addNodo(entregas.get(tmp.getInicial().getNodoId2() - 1));
                                    aux2.setDistancia(distancias);                                    
                                    N=aux2.getDistancia();
                                    Ahorro=R-N;                                    
                                    if(Ahorro > 0)
                                    {
                                        delRuta(aux2.getId(), tmp.getInicial().getNodoId2());                                        
                                        if(aux2.getTiempo()<12)
                                        {                                            
                                            ruta=aux2;                                              
                                        }                                        
                                    }
                                }
                                break;
                        }
                        break;
                    }
                    else if(tmp.getInicial() != null && nodo.getId()==tmp.getInicial().getNodoId2())
                    {                        
                        switch(ruta.iniciOFin(nodo.getId()))
                        {
                            case 1:
                                aux1=getRuta(ruta.getId(), tmp.getInicial().getNodoId1());
                                if(aux1 != null && aux1.getNodos().size() == 1)
                                {
                                    R = aux1.getDistancia() + ruta.getDistancia();
                                    aux2=ruta;
                                    aux2.addNodo(0, entregas.get(tmp.getInicial().getNodoId1() - 1));
                                    aux2.setDistancia(distancias);
                                    N=aux2.getDistancia();
                                    Ahorro=R-N;
                                    if(Ahorro > 0)
                                    {
                                        delRuta(aux2.getId(), tmp.getInicial().getNodoId1());
                                        if(aux2.getTiempo()<12)
                                        {
                                            ruta=aux2;                                            
                                        }                                        
                                    }
                                }
                                break;
                            case 2:
                                aux1=getRuta(ruta.getId(), tmp.getInicial().getNodoId1());
                                if(aux1 != null && aux1.getNodos().size() == 1)
                                {
                                    R = aux1.getDistancia() + ruta.getDistancia();
                                    aux2=ruta;
                                    aux2.addNodo(entregas.get(tmp.getInicial().getNodoId1() - 1));
                                    aux2.setDistancia(distancias);
                                    N=aux2.getDistancia();
                                    Ahorro=R-N;
                                    if(Ahorro > 0)
                                    {
                                        delRuta(aux2.getId(), tmp.getInicial().getNodoId1());
                                        if(aux2.getTiempo()<12)
                                        {
                                            ruta=aux2;                                            
                                        }                                        
                                    }
                                }
                                break;
                        }
                        break;
                    }                    
                }            
            }            
            if(tmp.getInicial() != null )
                tmp.delArco(tmp.getInicial().getId());
        }
    }
    
    private Ruta getRuta(int id_ruta, int id_nodo)
    {
        for (Ruta ruta : rutas)
        {
            for (Nodo nodo : ruta.getNodos())
            {
                if(nodo.getId() == id_nodo && ruta.getId() != id_ruta)
                {
                    return ruta;
                }
            }
        }
        return null;
    }
    
    private void delRuta(int id_ruta, int id_nodo)
    {
        for (int i = 0; i < rutas.size(); i++)
        {            
            for (Nodo nodo : rutas.get(i).getNodos())
            {
                if(nodo.getId() == id_nodo && id_ruta != rutas.get(i).getId())
                {                    
                    rutas.set(i, new Ruta(0));
                    break;
                }
            }
        }
    }        
}
