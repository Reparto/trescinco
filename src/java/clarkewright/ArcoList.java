package clarkewright;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aarón Velasco
 */
public class ArcoList
{
    private Arco inicial;
    private int secuencia;
    
    public ArcoList()
    {
        inicial=null;
        secuencia=1;
    }
    
    public void addArco(Arco arco)
    {
        arco.setId(secuencia);
        secuencia++;
        if(inicial==null)
            inicial=arco;
        else
        {
            if(inicial.getAhorro() < arco.getAhorro())
            {
                arco.next=inicial;
                inicial=arco;
            }
            else
            {
                Arco reco=inicial;
                Arco atras=inicial;
                while (reco.getAhorro() >= arco.getAhorro() && reco.next != null) 
                {
                    atras=reco;
                    reco=reco.next;
                }
                if (reco.getAhorro() >= arco.getAhorro()) 
                {
                    reco.next=arco;
                }
                else 
                {
                    arco.next=reco;
                    atras.next=arco;
                }                
            }
        }
    }
    public void delArco(int id)
    {
        if(inicial.getId() == id)
            inicial=inicial.next;
        else
        {
            Arco reco = inicial;
            Arco atras=inicial;
            while (reco != null)
            {
                if(reco.getId() == id)
                {
                    atras.next=reco.next;
                    break;
                }
                atras= reco;
                reco = reco.next;
            }
        }
    }
    
    public Arco findArco(int id1, int id2) 
    {
        Arco reco = inicial;
        while (reco != null) 
        {            
            if(reco.getNodoId1()==id1 || reco.getNodoId2()==id1 || reco.getNodoId1()==id2 || reco.getNodoId2()==id2)
                return reco;
        }        
        return null;
    }
    
    public void imprimir () 
    {
        Arco reco = inicial;
        while (reco != null) 
        {
            System.out.println(reco.getId()+" : "+reco.getNodoId1()+""+reco.getNodoId2()+" : "+reco.getAhorro());
            reco = reco.next;
        }
        System.out.println();
    }

    public Arco getInicial()
    {
        return inicial;
    }        
}
