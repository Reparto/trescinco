/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import clarkewright.Colonia;
import entidad.Lista;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aarón Velasco
 */
@Stateless
public class ColoniasFacade
{
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;
    
    public List<Colonia> getColonias(int bodegaUsu,String fecha) {
        Query query = em.createNativeQuery("SELECT distinct COL.id, COL.latitud, COL.longitud "
                + "FROM colonia AS COL  "
                + "JOIN (direccion AS D  "
                + "JOIN( clientes AS CLI  "
                + "JOIN(bodega AS B   "
                + "JOIN(sucursal AS S   "
                + "JOIN(venta AS V   "
                + "JOIN(lineaventa L   "
                + "JOIN maquinaria AS M ON L.MAQUINARIA=M.CODIGO)ON V.ID=L.VENTA)ON S.ID=V.SUCURSAL)ON B.ID=S.BODEGA)ON  CLI.rfc=V.clientes)ON D.id=CLI.direccion)ON COL.id=D.colonia  "
                + "WHERE B.ID=? AND V.FECHAESTIMADA = ? ORDER BY COL.colonia");

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        query.setParameter(1,bodegaUsu);
        query.setParameter(2,sdf.format(new Date(fecha)));    
        List<Object[]> results = query.getResultList();
        
        List<Colonia> colonias=new ArrayList();
        for (int i = 0; i <results.size(); i++) {
            Colonia obj=new Colonia();
            for (int j = 0; j < 11; j++) {
                switch  (j){
                    case 0:
                        int id = (int) results.get(i)[j];
                        obj.setId(id);
                        break;
                    case 1:
                        double lat= (double) results.get(i)[j];
                        obj.setLatitud(lat);
                        break;
                    case 2:
                        double lon= (double) results.get(i)[j];
                        obj.setLongitud(lon);
                        break;                     
                }
                  
            }
            colonias.add(obj);
        }        
       return colonias;        
    }    
}
