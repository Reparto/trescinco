/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import clarkewright.Cliente;
import clarkewright.Entrega;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aarón Velasco
 */
@Stateless
public class EntragaFacade
{
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;
    
    public List<Entrega> getEntregas(int bodegaUsu,String fecha) {
        Query query = em.createNativeQuery("SELECT CLI.rfc, D.calle, D.numero, COL.id, COL.colonia, M.nombre, L.CANTIDAD, (L.CANTIDAD*M.PESO)AS PESO_TOTAL   "
                + "FROM colonia AS COL  "
                + "JOIN (direccion AS D  "
                + "JOIN( clientes AS CLI  "
                + "JOIN(bodega AS B   "
                + "JOIN(sucursal AS S   "
                + "JOIN(venta AS V   "
                + "JOIN(lineaventa L   "
                + "JOIN maquinaria AS M ON L.MAQUINARIA=M.CODIGO)ON V.ID=L.VENTA)ON S.ID=V.SUCURSAL)ON B.ID=S.BODEGA)ON  CLI.rfc=V.clientes)ON D.id=CLI.direccion)ON COL.id=D.colonia  "
                + "WHERE B.ID= ? AND V.FECHAESTIMADA = ? ");

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        query.setParameter(1,bodegaUsu);
        query.setParameter(2,sdf.format(new Date(fecha)));            
        List<Object[]> results = query.getResultList();
        
        List<Entrega> entregas=new ArrayList();
        for (int i = 0; i <results.size(); i++) {
            Entrega obj=new Entrega();
            for (int j = 0; j < 11; j++) {
                                
                switch  (j){
                    case 0:
                        String id_cli= (String) results.get(i)[j];
                        obj.setId_cliente(id_cli);
                        break;                    
                    case 1:
                        String calle = (String) results.get(i)[j];
                        obj.setCalle(calle);
                        break;
                    case 2:
                        String numero= (String) results.get(i)[j];
                        obj.setNumero(numero);
                        break;
                    case 3:
                        int id_col= (int) results.get(i)[j];
                        obj.setId_colonia(id_col);
                        break;                    
                    case 4:
                        String colonia= (String) results.get(i)[j];
                        obj.setColonia(colonia);
                        break;                    
                    case 5:
                        String maquinaria= (String) results.get(i)[j];
                        obj.setMaquinaria(maquinaria);
                        break;                                            
                    case 6:
                        int cantidad= (int) results.get(i)[j];
                        obj.setCantidad(cantidad);
                        break;                                            
                    case 7:
                        double peso= (double) results.get(i)[j];
                        obj.setPeso(peso);
                        break;                                            
                }
                  
            }
            entregas.add(obj);
        }        
       return entregas;        
    }    
}
