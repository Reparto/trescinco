/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;


import entidad.Camion;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Faby
 */
@Stateless
public class CamionFacade extends AbstractFacade<Camion> {
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;
    
    public List<Camion> getCamiones(int id_bodega) {
        Query query = em.createNativeQuery("select id,capacidad  camion where bodega=?;");
        query.setParameter(1,id_bodega);
        List<Object[]> results = query.getResultList();
        
        List<Camion> camiones=new ArrayList();
        for (int i = 0; i <results.size(); i++) {
            Camion obj=new Camion();
            for (int j = 0; j < 11; j++) {
                switch  (j){
                    case 0:
                        int cant= (int) results.get(i)[j];
                        obj.setId(cant);
                        break;
                    case 1:
                        int cant1= (int) results.get(i)[j];
                        obj.setCapacidad(cant1);
                        break;
                                      
                }
                  
            }
            camiones.add(obj);
        }        
       return camiones;        
    }    
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CamionFacade() {
        super(Camion.class);
    }
    
}