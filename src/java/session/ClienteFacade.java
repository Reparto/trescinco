/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import clarkewright.Cliente;
import entidad.Lista;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aarón Velasco
 */
@Stateless
public class ClienteFacade
{
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;
    
    public List<Cliente> getClientes(int bodegaUsu,String fecha, int id_colonia) {
        Query query = em.createNativeQuery("SELECT CLI.rfc, L.CANTIDAD,(L.CANTIDAD*M.PESO)AS PESO_TOTAL    "
                + "FROM colonia AS COL  "
                + "JOIN (direccion AS D  "
                + "JOIN( clientes AS CLI  "
                + "JOIN(bodega AS B   "
                + "JOIN(sucursal AS S   "
                + "JOIN(venta AS V   "
                + "JOIN(lineaventa L   "
                + "JOIN maquinaria AS M ON L.MAQUINARIA=M.CODIGO)ON V.ID=L.VENTA)ON S.ID=V.SUCURSAL)ON B.ID=S.BODEGA)ON  CLI.rfc=V.clientes)ON D.id=CLI.direccion)ON COL.id=D.colonia  "
                + "WHERE B.ID= ? AND V.FECHAESTIMADA = ? and COL.id= ?");

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        query.setParameter(1,bodegaUsu);
        query.setParameter(2,sdf.format(new Date(fecha)));    
        query.setParameter(3,id_colonia);
        List<Object[]> results = query.getResultList();
        
        List<Cliente> clientes=new ArrayList();
        for (int i = 0; i <results.size(); i++) {
            Cliente obj=new Cliente();
            for (int j = 0; j < 11; j++) {
                switch  (j){
                    case 0:
                        String rfc = (String) results.get(i)[j];
                        obj.setRfc(rfc);
                        break;
                    case 1:
                        int cant= (int) results.get(i)[j];
                        obj.setCantidad(cant);
                        break;
                    case 2:
                        double peso= (double) results.get(i)[j];
                        obj.setPeso(peso);
                        break;                    
                }
                  
            }
            clientes.add(obj);
        }        
       return clientes;        
    }    
}
