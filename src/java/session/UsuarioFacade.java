/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entidad.Camion;
import entidad.Lista;
import entidad.Usuario;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Faby
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "trescincoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario findUser(String login,String pwd){
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", login);
        parameters.put("pwd", pwd);
    return super.findOneResult("Usuario.findByLogin", parameters);
    }
    
     public List<Lista> PorEntregar(int bodegaUsu,String fecha) {
        Query query = em.createNativeQuery("SELECT B.ID AS ID_BODEGA,COL.estado,COL.municipio,COL.colonia,L.VENTA, V.clientes AS ID_VENTA,M.CODIGO ,M.NOMBRE,L.CANTIDAD,M.PESO,(L.CANTIDAD*M.PESO)AS PESO_TOTAL  \n" +
        "FROM COLONIA AS COL\n" +
        "JOIN (DIRECCION AS D\n" +
        "JOIN( CLIENTES AS CLI\n" +
        "JOIN(BODEGA AS B \n" +
        "JOIN(SUCURSAL AS S \n" +
        "JOIN(VENTA AS V \n" +
        "JOIN(LINEAVENTA L \n" +
        "JOIN MAQUINARIA AS M ON L.MAQUINARIA=M.CODIGO)ON V.ID=L.VENTA)ON S.ID=V.SUCURSAL)ON B.ID=S.BODEGA)ON  CLI.rfc=V.clientes)ON D.id=CLI.direccion)ON COL.id=D.colonia\n" +
        "WHERE B.ID=? AND V.FECHAESTIMADA = ? ORDER BY COL.colonia");

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        query.setParameter(1,bodegaUsu);
        query.setParameter(2,sdf.format(new Date(fecha)));    
        List<Object[]> results = query.getResultList();
        
        List<Lista> lista=new ArrayList();
        for (int i = 0; i <results.size(); i++) {
            Lista obj=new Lista();
            for (int j = 0; j < 11; j++) {
                switch  (j){
                    case 0:
                        int op0 = (int) results.get(i)[j];
                        obj.setIdBodega(op0);
                        break;
                    case 4:
                        int op4= (int) results.get(i)[j];
                        obj.setIdVenta(op4);
                        break;
                    case 6:
                        int op6= (int) results.get(i)[j];
                        obj.setIdMaq(op6);
                        break;
                    case 8:    
                        int op8 = (int) results.get(i)[j];
                        obj.setCantidad(op8);
                        break;
                    case 1:
                        String op1 = (String) results.get(i)[j];
                        obj.setEstado(op1);
                        break;
                    case 2:
                        String op2 = (String) results.get(i)[j];
                        obj.setMunicipio(op2);
                        break;
                    case 3:
                        String op3 = (String) results.get(i)[j];
                        obj.setLocalidad(op3);
                        break;
                    case 5:
                        String op5 = (String) results.get(i)[j];
                        obj.setRfcCliente(op5);
                        break;
                    case 7:    
                        String op7 = (String) results.get(i)[j];
                        obj.setNomMaq(op7);
                        break;
                    case 9:
                        double op9 = (double) results.get(i)[j];
                        obj.setPeso(op9);
                        break;
                    case 10:
                        double op10 = (double) results.get(i)[j];
                        obj.setTotalPeso(op10);
                        break;     
                }
                  
            }
            lista.add(obj);
        }
        
       return lista;        
    }
}
